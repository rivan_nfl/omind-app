import React from 'react';
import './index.css';
import logo from '../../assets/logos/Top Logo.png';
import comp from '../../assets/images/Top Banner.png';
import tryout from '../../assets/images/Tryout.png';
import kuis from '../../assets/images/Kuis.png';
import listrik from '../../assets/icon/listrik.png';
import magnet from '../../assets/icon/magnetic.png';
import Turunan1 from '../../assets/icon/Turunan1.png';
import aljabar from '../../assets/icon/aljabar.png';
import present from '../../assets/icon/present.png';
import past from '../../assets/icon/past.png';
import pensil from '../../assets/icon/surface.png';
import lock from '../../assets/icon/lock.png';

const Dashboard = () => {
    return (
        <div>
            <div className='header'>
                <img src={logo} alt='Top Logo' />
                <div className='icons'>
                    <div className='icon' />
                    <div className='icon' />
                </div>
            </div>

            <div className='content'>
                <div className='top-banner'>
                    <div className='left-banner'>
                        <h1>Selamat Pagi, Sintya!</h1>
                        <p>Nikmati pengalaman belajar online paling lengkap, dan mudah dimengerti</p>
                    </div>
                    <img src={comp} />
                </div>

                <div className='stats'>
                    <img src={tryout} />
                    <img src={kuis} />
                </div>

                <div className='materi'>
                    <h3>Materi Saya</h3>
                    <div className='materi-wrapper'>

                        <div className='materi-card'>
                            <div className='left-materi'>
                                <h3>Medan Magnet</h3>
                                <p>Video . 13 jam lalu</p>
                            </div>
                            <img src={magnet} />
                        </div>

                        <div className='materi-card'>
                            <div className='left-materi'>
                                <h3>Listrik Statis</h3>
                                <p>Video . 13 jam lalu</p>
                            </div>
                            <img src={listrik} />
                        </div>

                        <div className='materi-card'>
                            <div className='left-materi'>
                                <h3>Turunan</h3>
                                <p>E-book . 13 jam lalu</p>
                            </div>
                            <img src={Turunan1} />
                        </div>

                        <div className='materi-card'>
                            <div className='left-materi'>
                                <h3>Aljabar</h3>
                                <p>Video . 13 jam lalu</p>
                            </div>
                            <img src={aljabar} />
                        </div>

                        <div className='materi-card'>
                            <div className='left-materi'>
                                <h3>Present Continuous</h3>
                                <p>Video . 13 jam lalu</p>
                            </div>
                            <img src={present} />
                        </div>

                        <div className='materi-card'>
                            <div className='left-materi'>
                                <h3>Simple Past Tense</h3>
                                <p>Video . 13 jam lalu</p>
                            </div>
                            <img src={past} />
                        </div>

                    </div>
                </div>

                <div>
                    <h3>Tryout Hari Ini</h3>
                    <div className='to-wrapper'>

                        <div className='to-card'>
                            <img src={pensil} />
                            <div className='to-right'>
                                <h3>Tryout UTBK 2021 #1</h3>
                                <p>25 - 25 Agustus 2020</p>
                            </div>
                        </div>

                        <div className='to-card'>
                            <div className='to-right'>
                                <h3>Tryout UTBK 2021 #1</h3>
                                <p>25 - 25 Agustus 2020</p>
                                <p style={{color: '#FFC446'}}>Rp10.000</p>
                            </div>
                            <img src={lock} />
                        </div>

                        <div className='to-card'>
                            <div className='to-right'>
                                <h3>Tryout UTBK 2021 #1</h3>
                                <p>25 - 25 Agustus 2020</p>
                                <p style={{color: '#FFC446'}}>Rp10.000</p>
                            </div>
                            <img src={lock} />
                        </div>


                    </div>
                </div>

            </div>
        </div>
    )
}

export default Dashboard;
