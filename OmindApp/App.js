import React from 'react';
import 'react-native-gesture-handler';

import { StatusBar } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';

import { Provider } from 'react-redux'
import store from './src/redux/store'

import AppStack from './src/navigators/AppStack';

const App = () => {
  return (
    <Provider store={store}>
      <StatusBar barStyle="dark-content" />
      <NavigationContainer>
        <AppStack />
      </NavigationContainer>
    </Provider>
  );
};

export default App;