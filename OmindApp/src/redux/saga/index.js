import { all } from 'redux-saga/effects';
import auth from './auth'
import grup from './grup'

export default function* rootSaga() {
    yield all([auth(), grup()])
}