import { takeLatest, put } from "redux-saga/effects";
import axios from 'axios';
import { baseUrl, getHeaders } from '../../common/auth';

function* createGroup(action) {
    try{
        const headers = yield getHeaders()

        const resGroup = yield axios({
            method: 'POST',
            url: `${baseUrl}/grup`,
            headers,
            data: action.payload
        })

        if(resGroup && resGroup.data) {
            console.log('Data Create Group : ', resGroup.data);
        }
        
        yield put({type: 'CREATE_GROUP_DONE'})
        yield put({type: 'DONE'})
        console.log('Create Group Success');
        
    } catch (err) {
        yield put({type: 'CREATE_GROUP_DONE'})
        console.log(err);
    }
}

function* groupSaga() {
    yield takeLatest('CREATE_GROUP', createGroup)
}

export default groupSaga;