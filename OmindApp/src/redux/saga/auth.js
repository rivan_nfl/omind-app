import { takeLatest, put } from 'redux-saga/effects'
import axios from 'axios'
import { baseUrl, saveToken, removeToken } from '../../common/auth'

function* login(action) {
    try{
        const resLogin = yield axios({
            method: 'POST',
            url: `${baseUrl}/tentor/login`,
            data: action.payload
        })

        if(resLogin && resLogin.data) {
            yield put({type: 'LOGIN_SUCCESS', payload: resLogin.data.data.user})
            yield saveToken(resLogin.data.data.token_access)
        }
        
        console.log('Login Success');

    } catch (err) {
        yield put({type: 'LOGIN_FAILED'})
        console.log('Login err :', err);
    }
}

function* logout() {
    try{
        yield removeToken()

    } catch (err) {
        console.log('Logout err:', err);
    }
}

function* authSaga() {
    yield takeLatest('LOGIN', login)
    yield takeLatest('LOGOUT', logout)
}

export default authSaga;