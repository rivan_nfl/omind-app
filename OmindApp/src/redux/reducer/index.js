import { combineReducers } from 'redux';
import auth from './auth'
import profile from './profile'
import grup from './grup'

export default combineReducers({
    auth, profile, grup
})