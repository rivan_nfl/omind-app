const initialState = {
    isLoading: false,
    isDone: false
}

const grup = (state = initialState, action) => {
    switch(action.type) {
        case 'CREATE_GROUP':
            return {
                ...state,
                isLoading: true
            }
        case 'CREATE_GROUP_DONE':
            return {
                ...state,
                isLoading: false
            }
        case 'DONE':
            return {
                ...state,
                isDone: true
            }
        case 'UNDONE':
            return {
                ...state,
                isDone: false
            }
        default:
            return state
    }
}

export default grup;