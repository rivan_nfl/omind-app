const initialState = {
    isLoggedIn: false,
    isLoading: false
}

const auth = (state = initialState, action) => {
    switch(action.type) {
        case 'LOGIN':
            return {
                ...state,
                isLoading: true
            }
        case 'LOGIN_SUCCESS':
            return {
                isLoading: false,
                isLoggedIn: true
            }
        case 'LOGIN_FAILED':
            return {
                ...state,
                isLoading: false,
            }
        case 'LOGOUT':
            return {
                ...state,
                isLoggedIn: false
            }
        default:
            return state
    }
}

export default auth;