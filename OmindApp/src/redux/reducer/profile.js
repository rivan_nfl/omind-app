const initialState = {
    username: null,
    email: null,
    nama: null,
    foto: null,
    lokasiMengajar: null
}

const profile = (state = initialState, action) => {
    switch(action.type) {
        case 'LOGIN_SUCCESS':
            return {
                username: action.payload.username,
                email: action.payload.email,
                nama: action.payload.nama,
                foto: action.payload.foto,
                lokasiMengajar: action.payload.lokasi_mengajar
            }
        case 'LOGOUT':
            return {
                username: null,
                email: null,
                nama: null,
                foto: null,
                lokasiMengajar: null
            }
        default:
            return state
    }
}

export default profile;