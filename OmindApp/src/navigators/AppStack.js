import React, { useSelector, } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { connect } from 'react-redux';

const Stack = createStackNavigator();

import LoginScreen from '../screens/LoginScreen';
import MainNavigator from './MainNavigator';

const AppStack = (props) => {
    // const verify = useSelector(state => {state.auth})

    // console.log(verify);

    return (
        <Stack.Navigator headerMode='none'>
            {
                props.verify
                ? <Stack.Screen name="Main" component={MainNavigator} />
                : <Stack.Screen name="Home" component={LoginScreen} />
            }
        </Stack.Navigator>
    )
}

const reduxState = (state) => ({
    verify: state.auth.isLoggedIn
})

const reduxDispatch = (dispatch) => ({

})

export default connect(reduxState, reduxDispatch)(AppStack)
// export default AppStack