import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

import HomeScreen from '../screens/HomeScreen';
import LiveScreen from '../screens/LiveScreen';
import DiscussionScreen from '../screens/DiscussionScreen';
import ProfileScreen from '../screens/ProfileScreen';

const MainNavigator = () => {
    return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarLabel: ({focused, color, size}) => {
                    let textColor;

                    if(route.name == 'Home') {
                    textColor = focused ? '#07689f' : 'gray'
                    } else if(route.name == 'Live') {
                    textColor = focused ? '#07689f' : 'gray'
                    } else if(route.name == 'Diskusi') {
                    textColor = focused ? '#07689f' : 'gray'
                    } else if(route.name == 'Profil') {
                    textColor = focused ? '#07689f' : 'gray'
                    }

                    return (
                    <Text style={{fontSize: 14, color: textColor, fontWeight: 'bold'}}>{route.name}</Text>
                    )
                },
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
        
                    if (route.name === 'Home') {
                      iconName = focused
                        ? 'home-outline'
                        : 'home-outline';
                    } else if (route.name === 'Live') {
                      iconName = focused ? 'radio-outline' : 'radio-outline';
                    } else if (route.name === 'Diskusi') {
                      iconName = focused ? 'chatbubbles-outline' : 'chatbubbles-outline';
                    } else if (route.name === 'Profil') {
                      iconName = focused ? 'person-outline' : 'person-outline';
                    }
                    return <Ionicons name={iconName} size={35} color={color} />;
                  },
            })}
            tabBarOptions={{
                activeTintColor: '#07689f',
                inactiveTintColor: 'gray',
                style: {height: 70},
                tabStyle: {paddingVertical: 7}
            }}
        >
            <Tab.Screen name="Home" component={HomeScreen} />
            <Tab.Screen name="Live" component={LiveScreen} />
            <Tab.Screen name="Diskusi" component={DiscussionScreen} />
            <Tab.Screen name="Profil" component={ProfileScreen} />
        </Tab.Navigator>
    )
}

export default MainNavigator
