import AsyncStorage from '@react-native-async-storage/async-storage';

export let baseUrl = 'http://bta70.omindtech.id/api';

export async function getHeaders() {
  const token = await AsyncStorage.getItem('TOKEN');

  return {
    'Content-Type': 'application/json',
    Authorization: token,
  };
}

export async function getAccountId() {
  return await AsyncStorage.getItem('ID');
}

export async function getToken() {
  return await AsyncStorage.getItem('TOKEN');
}

export async function saveToken(token) {
  AsyncStorage.setItem('TOKEN', token);
}

export async function saveAccountId(id) {
  AsyncStorage.setItem('ID', String(id));
}

export async function removeToken() {
  AsyncStorage.removeItem('TOKEN');
}

export async function removeAccountId() {
  AsyncStorage.removeItem('ID');
}

// react-native link @react-native-async-storage/async-storage