import React from 'react'
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import Header from '../components/Header';
import { connect } from 'react-redux'

const ProfileScreen = (props) => {
    return (
        <>
            <Header title='Profile' navigation={props.navigation} />
            <ScrollView>
                <View style={styles.container}>

                    <View style={styles.pp}>
                        <Icon name='person' size={100} color='#07689f' />
                    </View>

                    <Text style={styles.txtTitle}>Nama</Text>
                    <Text style={styles.txtDesc}>{props.profile.nama}</Text>
                    
                    <Text style={styles.txtTitle}>Username</Text>
                    <Text style={styles.txtDesc}>{props.profile.username}</Text>

                    <Text style={styles.txtTitle}>Email</Text>
                    <Text style={styles.txtDesc}>{props.profile.email}</Text>

                    <Text style={styles.txtTitle}>Lokasi Mengajar</Text>
                    <Text style={styles.txtDesc}>{props.profile.lokasiMengajar}</Text>

                    <View style={{marginTop: 30}}>
                        <Button title='Logout' onPress={props.logout} color='#07689f' />
                    </View>
                </View>
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 50,
        paddingBottom: 50,
        paddingHorizontal: 20
    },
    text: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    pp: {
        width: 140, 
        height: 140, 
        alignSelf: 'center', 
        borderWidth: 1,
        borderRadius: 100, 
        alignItems: 'center', 
        justifyContent: 'center',
        marginBottom: 40
    },
    txtTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    txtDesc: {
        backgroundColor: '#ddd',
        paddingHorizontal: 10,
        paddingVertical: 10,
        marginBottom: 10,
        borderRadius: 5,
        fontSize: 17
    },
})

const reduxState = (state) => ({
    profile: state.profile
})

const reduxDispatch = (dispatch) => ({
    logout: () => dispatch({type: 'LOGOUT'})
})

export default connect(reduxState, reduxDispatch)(ProfileScreen)