import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux'

const LoginScreen = (props) => {
    const [email, setEmail] = useState('tentor@gmail.com')
    const [password, setPassword] = useState('123123')

    const handleLogin = () => {
        if(!email) {
            alert('Email is Required')
        } else if(!password) {
            alert('Password is Required')
        } else {
            const data = new FormData()
            data.append('email', email)
            data.append('password', password)

            props.login(data)
        }
    }
    return (
        <ScrollView>
            <View style={styles.header}>
                <Text style={styles.headerText}>Login</Text>
            </View>
            <View style={styles.container}>

                <Icon name='radio-button-on-outline' size={100} color='#07689f' style={{alignSelf: 'center', marginBottom: 30}} />

                <TextInput placeholder='Email' placeholderTextColor='gray' style={styles.textInput} value={email} onChangeText={text => setEmail(text)} />

                <TextInput placeholder='Password' placeholderTextColor='gray' style={styles.textInput} secureTextEntry={true} value={password} onChangeText={text => setPassword(text)} />

                {
                    props.loading
                    ? (
                        <TouchableOpacity style={styles.btnWrapper} >
                            <ActivityIndicator size={23} color='white' />
                        </TouchableOpacity>
                    )
                    : (
                        <TouchableOpacity style={styles.btnWrapper} onPress={() => handleLogin()} >
                            <Text style={styles.btnText}>Login</Text>
                        </TouchableOpacity>
                    )
                }
                
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 70,
        paddingBottom: 50,
        paddingHorizontal: 50
    },
    textInput: {
        backgroundColor: '#ddd',
        marginBottom: 10,
        borderRadius: 5,
        paddingHorizontal: 10,
        fontSize: 17
    },
    btnWrapper: {
        backgroundColor: '#07689f',
        paddingVertical: 10,
        borderRadius: 8,
        marginTop: 40
    },
    btnText: {
        fontSize: 17,
        color: 'white',
        textAlign: 'center'
    },
    header: {
        backgroundColor: '#07689f',
        paddingVertical: 15,
        paddingHorizontal: 16
    },
    headerText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    }
})

const reduxState = (state) => ({
    loading: state.auth.isLoading
})

const reduxDispatch = (dispatch) => ({
    login: (payload) => dispatch({type: 'LOGIN', payload})
})

export default connect(reduxState, reduxDispatch)(LoginScreen)