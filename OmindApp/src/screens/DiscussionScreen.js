import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, Alert, ScrollView, ActivityIndicator, Image } from 'react-native'
import Header from '../components/Header';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';

const options = {
    title: 'Select Avatar',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
};

const DiscussionScreen = (props) => {
    const [image, setImage] = useState();
    const [rawImage, setRawImage] = useState();
    const [id, setId] = useState();
    const [nama, setNama] = useState();

    function pickImage() {
        ImagePicker.showImagePicker(options, (response) => {
            // console.log('Response = ', response);
            
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = {
                uri: response.uri,
                type: response.type,
                data: response.data,
                name: response.fileName
                };
            
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                setRawImage(source)
                setImage(response.uri)
            }
        });
    }
    
    async function uploadImage(){
        let token = await getToken()
    
        const data = new FormData();
        data.append('file', rawImage)
        Axios({
            method: 'POST',
            url: `${baseUrl}files`,
            data: data,
            headers: {
                'Content-Type' : 'multipart/form-data',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token
            }
        })
        .then((res) => {
            setImage(res.data.imageUrl)
            console.log('Image Uploaded');
        })
        .catch((err)=>{
            console.error(JSON.stringify(err))
        })
    }

    const handleCreateGroup = () => {
        if(!nama) {
            alert('Nama Group Diperlukan')
        } else if(!id) {
            alert('Jenjang Diperlukan')
        } else if(id == 'jenjang') {
            alert('Silahkan Pilih Jenjang')
        } else if(!image) {
            alert('Gambar Diperlukan')
        } else {
            const data = new FormData()
            data.append('kelas_id', id)
            data.append('nama', nama)
            data.append('thumbnail', rawImage)

            props.createGroup(data)
        }
    }

    return (
        <>
            <Header title='Buat Grup' navigation={props.navigation} />

            <ScrollView>
                <View style={styles.container}>

                    {
                        image
                        ? <>
                            <View style={styles.img}>
                                <Image source={{uri: image}} resizeMode='contain' style={{width: '100%', height: 150}} />
                            </View>

                            <TouchableOpacity
                            style={{alignSelf: 'center', marginTop: 10}}
                            onPress={pickImage}>
                                <Text style={{fontWeight: 'bold', fontSize: 17, textDecorationLine: 'underline', color: '#07689f'}}>Ganti Foto</Text>
                            </TouchableOpacity>
                        </>
                        : <>
                            <TouchableOpacity onPress={pickImage}>
                                <View style={styles.photoWrapper}>
                                    <Icon name='camera' size={50} color='black' />
                                </View>
                            </TouchableOpacity>
                            
                            <Text style={styles.addPhoto}>Tambahkan foto grup</Text>
                        </>
                    }


                    <TextInput placeholder='Nama Grup' style={styles.textInput} placeholderTextColor='grey' value={nama} onChangeText={text => setNama(text)} />

                    <View style={styles.dd}>
                        <Picker
                            selectedValue={id}
                            style={{height: '100%'}}
                            onValueChange={itemValue => setId(itemValue)}
                        >
                            <Picker.Item label="Jenjang" value="jenjang" color='grey' />
                            <Picker.Item label="SD" value="1" />
                            <Picker.Item label="SMP" value="2" />
                            <Picker.Item label="SMA" value="3" />
                        </Picker>
                    </View>

                    {
                        props.loading
                        ? (
                            <TouchableOpacity style={styles.btnWrapper}>
                                <ActivityIndicator size={23} color='white' />
                            </TouchableOpacity>
                        )
                        : (
                            <TouchableOpacity onPress={handleCreateGroup} style={styles.btnWrapper} >
                                <Text style={styles.btnText}>Buat Grup</Text>
                            </TouchableOpacity>
                        )
                    }
                    
                </View>
                
            </ScrollView>
            {
                props.task
                ? Alert.alert('SUCCESS', 'Sukses Membuat Grup', [{text:'Tutup', onPress: props.unDone}, {text: 'Kembali ke Home', onPress: () => {
                    props.navigation.navigate('Home')
                    props.unDone()
                }}])
                : null
            }
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 50,
        paddingVertical: 50
    },
    photoWrapper: {
        backgroundColor: '#ddd',
        borderRadius: 5,
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    addPhoto: {
        textAlign: 'center',
        marginTop: 10,
        fontSize: 17
    },
    textInput: {
        paddingVertical: 13,
        paddingHorizontal: 10,
        backgroundColor: '#ddd',
        fontSize: 17,
        marginVertical: 20,
        borderRadius: 5
    },
    dd: {
        height: 40,
        width: '40%',
        marginBottom: 50,
        backgroundColor: '#ddd',
        borderRadius: 5
    },
    btnWrapper: {
        backgroundColor: '#07689f',
        width: '100%',
        paddingVertical: 10,
        borderRadius: 8,
        alignItems: 'center',
    },
    btnText: {
        fontSize: 17,
        color: 'white'
    },
    img: {
        width: '100%',
        alignSelf: 'center',
    }
})

const reduxState = (state) => ({
    task: state.grup.isDone,
    loading: state.grup.isLoading,
})

const reduxDispatch = (dispatch) => ({
    createGroup: payload => dispatch({type: 'CREATE_GROUP', payload}),
    unDone: () => dispatch({type: 'UNDONE'})
})

export default connect(reduxState, reduxDispatch)(DiscussionScreen);