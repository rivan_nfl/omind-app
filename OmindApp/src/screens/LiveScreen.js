import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import Header from '../components/Header';

const LiveScreen = (props) => {
    return (
        <>
            <Header title='Live' navigation={props.navigation} />
            <View style={styles.container}>
                <Icon name='radio-outline' size={100} color='#07689f' />
                <Text style={styles.text}>Live Screen</Text>
            </View>
        </>
    )
}

export default LiveScreen

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 30,
        fontWeight: 'bold'
    }
})
